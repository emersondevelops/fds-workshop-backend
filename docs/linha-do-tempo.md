# Linha do Tempo

> [Voltar para a Página Principal](../README.md)

01. [Pré-Workshop](#pr-workshop)
02. [Introdução](#introduo)
03. [Fundamentos](#fundamentos)
04. [Entendendo o HTTP](#entendendo-o-http)
05. [Comunicação com banco de dados](#comunicao-com-banco-de-dados)
06. [Models](#models)
07. [Views](#views)
08. [URLs](#urls)
09. [Templates](#templates)
10. [Inserindo clientes](#inserindo-clientes)
11. [Trabalhando com templates](#trabalhando-com-templates)
12. [Exibindo cliente por ID](#exibindo-cliente-por-id)
13. [Editando clientes](#editando-clientes)
14. [Removendo clientes](#removendo-clientes)
15. [Utilizando o Service Layer](#utilizando-o-service-layer)
16. [Segurança](#segurana)
17. [Conclusão](#concluso)

## Pré-Workshop
- [x] ~~Passar um [formulário de nivelamento](https://forms.gle/NJJL4gr1hyhKaDAa6)~~ Conversar sobre o que já sabem no Whatsapp
- [x] Fazer um vídeo dos pré-requisitos ensinando a instalar o Python e o Pycharm no Windows e no Linux
- [x] Indicar um curso introdutório: [Curso de Python do Curso em Vídeo](https://www.youtube.com/playlist?list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6)
- [x] Interação de boas-vindas e conhecer os participantes
- [x] Ensinando a como tirar dúvida utilizando uma plataforma externa para compartilhar vídeo ([Discord](https://discord.com/)) ou para postar código ([Gitlab Snippets](https://gitlab.com/dashboard/snippets))

## Introdução

- [x] Preparando o ambiente: [Git](https://git-scm.com/), [Python](https://www.python.org/), [PyCharm](https://www.jetbrains.com/pt-br/pycharm/), [Postgres](https://www.postgresql.org/) e [Postbird](https://www.electronjs.org/apps/postbird) instalados na máquina
- [x] Criando o projeto: pasta com GIT e VENV, e [DJANGO-ADMIN](https://www.djangoproject.com/)
- [x] [Projetos VS Apps: explicar que um projeto django é composto por vários apps](intro/projetos-vs-apps.md)
- [x] Criando um APP: criar um app pessoa
- [x] Executando o projeto: utilizando o ```python manage.py runserver```

## Fundamentos

- [x] [O padrão MTV - Model-Template-View](fundamentos/padrao-model-template-view.md)
- [x] [Estrutura básica de um projeto](fundamentos/estrutura-projeto.md)

## Entendendo o HTTP

- [ ] [Mas o que é, afinal, o HTTP?](http/http.md)

## Comunicação com banco de dados

- [x] Conectando o projeto ao PostgreSQL: instalar o driver e configurar
- [x] Conectando a outros bancos de dados: [consultar documentação do Django](https://docs.djangoproject.com/en/3.1/ref/databases/)

## Models

- [x] [Para quê serve a camada model?](models/model.md)
- [x] Criando o primeiro model: cria o model Client com name, date_of_birth, email, occupation, gender e definir uma função str
- [x] Entendendo e migrando os models: utilizar os comandos da CLI, exibir o arquivo criado e mostrar no banco de dados

## Views

- [x] [Para quê serve a camada View?](views/view.md)
- [ ] [Entendendo o parâmetro request](views/request.md)
- [ ] [Entendendo o parâmetro response](views/response.md)
- [x] Criando a primeira View para listar clientes

## URLs

- [x] Para quê servem as urls?
- [x] Criando as urls para retornar as views: criar arquivo urls.py e integrar com o correspondente do projeto

## Templates

- [x] [Para quê serve a camada Template?](templates/template.md)
- [x] Exibindo os dados obtidos pela view através do template: configurar o diretório do template, e criar o arquivo HTML

## Inserindo clientes

- [ ] Criando o forms.py de clientes: criar o form ClientForm com todos os campos
- [ ] Criando o formulário para inserir um novo cliente: criar o template HTML
- [ ] Para quê serve o csrf_token	Explicar que é umas das formas de proteção contra ataques
- [ ] Persistindo as informações do formulário no BD	Inserir código na view correspondente
- [ ] Exibindo mensagens de validação no formulário: explicar a validação no frontend, no backend e inserir as tags após o input

## Trabalhando com templates

- [ ] Estendendo templates: criar o template base HTML e estender
- [ ] Utilizando arquivos estáticos: configurar pasta dos arquivos estáticos e inserir uma logomarca
- [ ] Traduzindo a aplicação: mudando no settings.py a linguagem para pt-br

## Exibindo cliente por ID

- [ ] Criando método para buscar cliente por ID: criar view para buscar cliente por ID
- [ ] Exbindo informações do cliente no template: criar o template HTML

## Editando clientes

- [ ] Buscando e exibindo informações do cliente no formulário: criar a view para editar o cliente
- [ ] Persistindo as informações do formulário no BD: inserir código na view de editar o cliente para persistir os dados salvos

## Removendo clientes

- [ ] Criando o método para remover clientes do BD: criar a view para remover o cliente
- [ ] Chamando o método para remover cliente no template: inserir código na view de remover o cliente para realizar a ação

## Utilizando o Service Layer

- [ ] Criar camada services para refatorar a view: pega as queries e joga para o service

## Segurança

- [ ] Prevenindo SQL Injection: explicar como pode acontecer este ataque (SQL Query)
- [ ] Prevenindo XSS: explicar como pode acontecer este ataque (tags e scripts são apenas strings)
- [ ] Prevenindo CSRF: explicar como pode acontecer este ataque (@csrf_exempt, impede requisições advindas de outra aplicação)

## Conclusão
- [ ] Agradecer e interagir sobre o que foi aprendido
