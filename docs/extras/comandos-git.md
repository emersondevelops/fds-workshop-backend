# Comandos Git

> [Voltar para a Página Principal](../README.md)

- **git init**: inicia um repositório git
- **git add <arquivo, pasta ou tudo (usando .)>**: adiciona para área de arquivos a serem commitados
- **git commit -m <mensagem>**: commita os arquivos com uma mensagem
- **git remote add origin <link do repositório>**: adiciona o repositório git online no local
- **git push origin master**: envia os commits do repositório local para o repositório online na branch master
- **git reset HEAD~<números de commits a serem voltados>**: reverte commits
- **git reset <arquivo, pasta, ou tudo (usando .)>**: remove arquivos da área de commitação
