# Comandos do terminal do Windows

> [Voltar para a Página Principal](../README.md)

```cd```
: **Change Directory**, muda em que pasta você está. Exemplos: para voltar uma pasta (```cd ..```), para ir para uma pasta dentro da atual que se chama www (```cd www```)

```mkdir```
: **Make Directory**, cria uma pasta dentro da pasta que você está. Exemplo: (```mkdir workshop-backend```)

```del```
: **Delete**, deleta um arquivo. Exemplo (```del requirements.txt```)

```rmdir /s /q```
: **Remove Directory**, remove uma pasta, suas subpastas e sem confirmação. Exemplo (```rmdir .git```)

```dir```
: **Directory**, lista o que tem dentro da pasta.